import React from 'react';
import BurgerIngredient from './BurgerIngedient/BurgerIngredient';
import classes from './Burger.module.css';

const burger = (props) => {
    let ingredientArr = Object.keys(props.ingredients)
        .map(igKey => {
            return [...Array(props.ingredients[igKey])].map((_, i) => {
                return <BurgerIngredient key={igKey + i} type={igKey} />;
            });
        })
        .reduce((arr, el) => {
            return arr.concat(el)
        }, []);

    if (ingredientArr.length === 0) {
        ingredientArr = <p>This is an empty bun</p>
    }

    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {ingredientArr}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

export default burger;