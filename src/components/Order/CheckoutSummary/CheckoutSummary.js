import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
import classes from './CheckoutSummary.module.css';

const CheckoutSummary = (props) => {

    return (
        <div className={classes.CheckoutSummary}>
            <h1>Order 5 minutes before deliveries stop, to get bonus ingredients</h1>
            <div style={{width: '100%', margin: 'auto'}}>
                <Burger ingredients={props.ingredients}/>
            </div>
            <Button 
                btnType="Danger"
                clicked={props.checkoutCancelled}>CANCEL</Button>
            <Button 
                btnType="Success" 
                clicked={props.checkoutContinued}>CONTINUE</Button>
        </div>
    );
}
    

export default CheckoutSummary;