import reducer from './auth';
import * as actionTypes from '../actions/actionTypes';

describe('auth reducer', () => {

    let initState = {
        token: null,
        userId: null,
        error: null,
        loading: false,
        authRedirectPath: '/'
    }

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initState);
    });

    it('should store the token upon login', () => {
        expect(reducer(initState, {
            type: actionTypes.AUTH_SUCCESS,
            idToken: 's0m3-t0k3n',
            userId: 's0m3-u53r-1d'
        })).toEqual({
            token: 's0m3-t0k3n',
            userId: 's0m3-u53r-1d',
            error: null,
            loading: false,
            authRedirectPath: '/'
        })
    });
});